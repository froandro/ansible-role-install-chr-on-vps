Ansible_role_install_chr_on_vps 
=========

Роль позволяет развернуть программный CHR Mikrotik на VPS

Requirements
------------

- VPS c Ubuntu

Role Variables
--------------

Переменные определены в файле defaults/main.yml:
```
- url: https://download.mikrotik.com/routeros/6.48.6/chr-6.48.6.img.zip # ссылка на архив образа CHR v.6.48.6
- target_disk: /dev/sda # раздел VPS Ubuntu, на который будет устанавливаться CHR; замените под свои нужды, определив его предварительно командой `fdisk -l` на целевом хосте
```

Example Playbook
----------------

Пример плейбука применительно к хостам группы `VPS_Ubuntu`:
```
    - hosts: VPS_Ubuntu
      roles:
         - ansible_role_install_chr_on_vps 
```

После выполнения плейбука, на стороне хостера VPS, необходимо в панели управления выключить виртуальную машину, выбрать загрузку с диска и запустить виртуальную машину снова. 
После чего должна начаться загрузка CHR Mikrotik.
Установка CHR Mikrotik на VPS Ubuntu завершена. 
Можно приступать к непосредственным настройкам самого CHR (вручную, скриптами или с помощью Ansible-роли, например https://gitlab.com/froandro/ansible-role-setup-chr-mikrotik.git).  

License
-------

BSD

Author Information
------------------
AlZa
